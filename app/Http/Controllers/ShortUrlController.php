<?php

namespace App\Http\Controllers;

use App\Http\Requests\ShortUrlCreateRequest;
use App\Models\ShortUrl;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ShortUrlController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $shortUrls = ShortUrl::all();

        return response()->json($shortUrls);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ShortUrlCreateRequest  $request
     * @return JsonResponse
     */
    public function store(ShortUrlCreateRequest $request)
    {
        $originalUrl = $request->get('original_url');
        $shortenUrls = ShortUrl::pluck('short_url');
        $shortenUrl = 'QW0wj';

        $shortUrl = ShortUrl::where('original_url',$originalUrl)->first();

        if ($shortUrl) {
            return response()->json(['errors' => ['Url already exist']],422);
        }

        while ($shortenUrls->contains($shortenUrl)) {
            $shortenUrl = Str::random(5);
        }

        $shortUrl = ShortUrl::create([
            'original_url' => $originalUrl,
            'short_url' => $shortenUrl,
        ]);

        return response()->json($shortUrl);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ShortUrl  $shortUrl
     * @return \Illuminate\Http\Response
     */
    public function show(ShortUrl $shortUrl)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ShortUrl  $shortUrl
     * @return \Illuminate\Http\Response
     */
    public function edit(ShortUrl $shortUrl)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ShortUrl  $shortUrl
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ShortUrl $shortUrl)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ShortUrl  $shortUrl
     * @return \Illuminate\Http\Response
     */
    public function destroy(ShortUrl $shortUrl)
    {
        //
    }
}
